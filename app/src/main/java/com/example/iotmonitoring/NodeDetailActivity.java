package com.example.iotmonitoring;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.iotmonitoring.adapter.NodeTimestampAdapter;
import com.example.iotmonitoring.model.NodeModel;
import com.example.iotmonitoring.model.NodeTimestampModel;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;

public class NodeDetailActivity extends AppCompatActivity {

    public static final String EXTRA_NODE = "extra_node";
    private NodeModel node;

    private TextView tvName, tvDescription, tvKey;
    private String getName, getDescription, getKey;
    private FloatingActionButton fabModify, fabEdit, fabDelete;
    private TextView tvEdit, tvDelete;
    private Boolean isAllFabsVisible;
    private ProgressBar progressBar;

    private FirebaseAuth mAuth;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference reference;
    private ArrayList<NodeTimestampModel> dataTimestamps;
    private NodeTimestampAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView rvTimestamps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_node_detail);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Node Details");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        tvName = findViewById(R.id.tv_node_name);
        tvDescription = findViewById(R.id.tv_node_description);
        tvKey = findViewById(R.id.tv_node_key);

        fabModify = findViewById(R.id.fab_modify_node);
        fabEdit = findViewById(R.id.fab_edit_node);
        fabDelete = findViewById(R.id.fab_delete_node);
        tvEdit = findViewById(R.id.tv_edit_node);
        tvDelete = findViewById(R.id.tv_delete_node);
        progressBar = findViewById(R.id.pb_node_detail);
        rvTimestamps = findViewById(R.id.rv_timestamps);

        fabEdit.setVisibility(View.GONE);
        fabDelete.setVisibility(View.GONE);
        tvEdit.setVisibility(View.GONE);
        tvDelete.setVisibility(View.GONE);
        isAllFabsVisible = false;

        mAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();
        reference = firebaseDatabase.getReference();
        myRecyclerView();

        if (getIntent().getExtras() != null) {
            fabModify.setOnClickListener(new View.OnClickListener() {
                @SuppressLint("UseCompatLoadingForDrawables")
                @Override
                public void onClick(View view) {
                    if (!isAllFabsVisible) {
                        fabModify.setImageDrawable(getResources().getDrawable(R.drawable.ic_close));
                        fabEdit.show();
                        fabDelete.show();
                        tvEdit.setVisibility(View.VISIBLE);
                        tvDelete.setVisibility(View.VISIBLE);
                        isAllFabsVisible = true;
                    } else {
                        fabModify.setImageDrawable(getResources().getDrawable(R.drawable.ic_menu));
                        fabEdit.hide();
                        fabDelete.hide();
                        tvEdit.setVisibility(View.GONE);
                        tvDelete.setVisibility(View.GONE);
                        isAllFabsVisible = false;
                    }
                }
            });
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        node = getIntent().getParcelableExtra(EXTRA_NODE);
        if (node != null) {
            getData(node);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void getData(NodeModel node) {
        showLoading(true);
        getKey = node.getKey();
        tvKey.setText(getKey);

        reference.child("users").child(mAuth.getUid()).child("nodes").child(getKey)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        NodeModel nodeModel = dataSnapshot.getValue(NodeModel.class);
                        dataTimestamps = new ArrayList<>();
                        for (DataSnapshot timeSnapshot : dataSnapshot.child("data").getChildren()) {
                            NodeTimestampModel timestamps = new NodeTimestampModel(
                                    timeSnapshot.getKey()
                            );
                            dataTimestamps.add(timestamps);
                        }
                        Collections.reverse(dataTimestamps);
                        adapter = new NodeTimestampAdapter(dataTimestamps, getKey,
                                NodeDetailActivity.this);
                        rvTimestamps.setAdapter(adapter);

                        try {
                            nodeModel.setKey(getKey);
                            getName = nodeModel.getName();
                            getDescription = nodeModel.getDescription();
                            tvName.setText(getName);
                            tvDescription.setText(getDescription);

//                            fab listener must added after retreat new data
                            fabEdit.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Intent intent = new Intent(view.getContext(),
                                            NodeInsertUpdateActivity.class);
                                    intent.putExtra(NodeDetailActivity.EXTRA_NODE, nodeModel);
                                    startActivity(intent);
                                }
                            });

                            fabDelete.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    AlertDialog.Builder alertDialogBuilder = new AlertDialog
                                            .Builder(NodeDetailActivity.this);
                                    alertDialogBuilder.setTitle("Delete data");
                                    alertDialogBuilder
                                            .setMessage("Are you sure want to delete this data?")
                                            .setCancelable(false)
                                            .setPositiveButton("Yes",
                                                    (dialog, id) -> {
                                                        onDeleteData(node);
                                                        finish();
                                                    })
                                            .setNegativeButton("No",
                                                    (dialog, id) -> dialog.cancel());
                                    AlertDialog alertDialog = alertDialogBuilder.create();
                                    alertDialog.show();
                                }
                            });

                        } catch (Exception exception) {

                        }
                        showLoading(false);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Toast.makeText(getApplicationContext(),
                                "Fail to load data", Toast.LENGTH_LONG).show();
                        Log.e("NodesActivity", databaseError.getDetails()
                                + " " + databaseError.getMessage());
                        showLoading(false);
                    }
                });
    }

    public void onDeleteData(NodeModel node) {
        showLoading(true);
        String userId = mAuth.getCurrentUser().getUid();
        if (reference != null) {
            reference.child("users").child(userId).child("nodes").child(node.getKey()).removeValue()
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Toast.makeText(NodeDetailActivity.this,
                                    "Data successfully deleted", Toast.LENGTH_SHORT).show();
                            finish();
                            showLoading(false);
                        }
                    })
                    .addOnFailureListener(this, new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(NodeDetailActivity.this,
                                    "Data failed to delete", Toast.LENGTH_SHORT).show();
                            finish();
                            showLoading(false);
                        }
                    });
        }
    }

    private void myRecyclerView() {
        layoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false);
        rvTimestamps.setLayoutManager(layoutManager);
        rvTimestamps.setHasFixedSize(true);
    }

    private void showLoading(Boolean isLoading) {
        if (isLoading) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }
}