package com.example.iotmonitoring;

import android.content.Intent;
import android.os.Bundle;

import com.example.iotmonitoring.adapter.NodeAdapter;
import com.example.iotmonitoring.model.NodeModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class NodeActivity extends AppCompatActivity {

    private TextView tvName, tvEmail;
    private FloatingActionButton fabAddNode;
    private Button btnLogout;
    private RecyclerView rvNodes;
    private RecyclerView.LayoutManager layoutManager;
    private ProgressBar progressBar;

    private FirebaseAuth mAuth;
    private DatabaseReference reference;
    private ArrayList<NodeModel> dataNodes;
    private NodeAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_node);

        tvName = findViewById(R.id.tv_name);
        tvEmail = findViewById(R.id.tv_email);
        rvNodes = findViewById(R.id.rv_nodes);
        progressBar = findViewById(R.id.pb_nodes);

        mAuth = FirebaseAuth.getInstance();
        myRecyclerView();
        getData();

        fabAddNode = findViewById(R.id.fab_add_node);
        fabAddNode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(
                        NodeActivity.this, NodeInsertUpdateActivity.class);
                startActivity(intent);
            }
        });

        btnLogout = findViewById(R.id.btn_logout);
        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog
                        .Builder(NodeActivity.this);
                alertDialogBuilder.setTitle("Sign Out");
                alertDialogBuilder
                        .setMessage("Are you sure want to sign out?")
                        .setCancelable(false)
                        .setPositiveButton("Yes",
                                (dialog, id) -> {
                                    mAuth.signOut();
                                    Intent intent = new Intent(
                                            NodeActivity.this, MainActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                                            | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                })
                        .setNegativeButton("No", (dialog, id) -> dialog.cancel());
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null) {
            tvName.setText(currentUser.getDisplayName());
            tvEmail.setText(currentUser.getEmail());
        }
    }

    private void myRecyclerView() {
        layoutManager = new LinearLayoutManager(this);
        rvNodes.setLayoutManager(layoutManager);
        rvNodes.setHasFixedSize(true);
    }

    private void getData() {
        showLoading(true);
        reference = FirebaseDatabase.getInstance().getReference();
        reference.child("users").child(mAuth.getUid()).child("nodes")
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        dataNodes = new ArrayList<>();
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            NodeModel nodes = snapshot.getValue(NodeModel.class);
                            nodes.setKey(snapshot.getKey());
                            dataNodes.add(nodes);
                        }
                        adapter = new NodeAdapter(dataNodes, NodeActivity.this);
                        rvNodes.setAdapter(adapter);
                        showLoading(false);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Toast.makeText(getApplicationContext(),
                                "Fail to load data", Toast.LENGTH_LONG).show();
                        Log.e("NodesActivity", databaseError.getDetails()
                                + " " + databaseError.getMessage());
                        showLoading(false);
                    }
                });
    }

    private void showLoading(Boolean isLoading) {
        if (isLoading) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }
}