package com.example.iotmonitoring;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.iotmonitoring.model.NodeModel;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

public class NodeInsertUpdateActivity extends AppCompatActivity {

    public static final String EXTRA_NODE = "extra_node";
    private boolean isEdit = false;
    private NodeModel node;

    private EditText etName, etDescription;
    private Button btnSubmit;
    private ProgressBar progressBar;

    private FirebaseAuth mAuth;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_node_insert_update);

        etName = findViewById(R.id.et_name);
        etDescription = findViewById(R.id.et_description);
        btnSubmit = findViewById(R.id.btn_submit);
        progressBar = findViewById(R.id.pb_node_iu);

        mAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();
        reference = firebaseDatabase.getReference();

        String actionBarTitle = null;
        String btnTitle = null;

        node = getIntent().getParcelableExtra(EXTRA_NODE);
        if (node != null) {
            isEdit = true;
            actionBarTitle = "Update Node";
            btnTitle = "Update";
            etName.setText(node.getName());
            etDescription.setText(node.getDescription());
        } else {
            actionBarTitle = "Create New Node";
            btnTitle = "Create";
            node = new NodeModel();
        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(actionBarTitle);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        btnSubmit.setText(btnTitle);
        btnSubmit.setOnClickListener(view -> {
            showLoading(true);
            String name = etName.getText().toString().trim();
            String description = etDescription.getText().toString().trim();
            if (name.isEmpty()) {
                etName.setError("Field can not be blank");
            } else if (description.isEmpty()) {
                etDescription.setError("Field can not be blank");
            } else {
                node.setName(name);
                node.setDescription(description);
                if (isEdit) {
                    updateNode(node);
                } else {
                    createNode(node);
                }
                finish();
            }
            showLoading(false);
        });
    }

    private void createNode(NodeModel node) {
        showLoading(true);
        String userId = mAuth.getCurrentUser().getUid();
        reference.child("users").child(userId).child("nodes").push().setValue(node)
                .addOnSuccessListener(this, new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void unused) {
                        Toast.makeText(NodeInsertUpdateActivity.this,
                                "Data successfully created", Toast.LENGTH_SHORT).show();
                        finish();
                        showLoading(false);
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(NodeInsertUpdateActivity.this,
                                "Data failed to create", Toast.LENGTH_SHORT).show();
                        finish();
                        showLoading(false);
                    }
                });
    }

    private void updateNode(NodeModel node) {
        showLoading(true);
        String userId = mAuth.getCurrentUser().getUid();
        String getKey = node.getKey();
        String getName = node.getName();
        String getDescription = node.getDescription();

        Map<String, Object> updates = new HashMap<String, Object>();
        updates.put("name", getName);
        updates.put("description", getDescription);

        reference.child("users").child(userId).child("nodes").child(getKey).updateChildren(updates)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        etName.setText("");
                        etDescription.setText("");
                        node.setName(node.getName());
                        node.setDescription(node.getDescription());
                        Toast.makeText(NodeInsertUpdateActivity.this,
                                "Data successfully updated", Toast.LENGTH_SHORT).show();
                        finish();
                        showLoading(false);
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(NodeInsertUpdateActivity.this,
                                "Data failed to update", Toast.LENGTH_SHORT).show();
                        finish();
                        showLoading(false);
                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showLoading(Boolean isLoading) {
        if (isLoading) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }
}