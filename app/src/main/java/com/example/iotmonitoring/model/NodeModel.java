package com.example.iotmonitoring.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class NodeModel implements Parcelable {
    private String key;
    private String name;
    private String description;

    public NodeModel() {
    }

    public NodeModel(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.key);
        dest.writeString(this.name);
        dest.writeString(this.description);
    }

    public void readFromParcel(Parcel source) {
        this.key = source.readString();
        this.name = source.readString();
        this.description = source.readString();
    }

    protected NodeModel(Parcel in) {
        this.key = in.readString();
        this.name = in.readString();
        this.description = in.readString();
    }

    public static final Parcelable.Creator<NodeModel> CREATOR = new Parcelable.Creator<NodeModel>() {
        @Override
        public NodeModel createFromParcel(Parcel source) {
            return new NodeModel(source);
        }

        @Override
        public NodeModel[] newArray(int size) {
            return new NodeModel[size];
        }
    };
}
