package com.example.iotmonitoring.model;


public class NodeTimestampModel {
    private String timestamp;

    public NodeTimestampModel() {
    }

    public NodeTimestampModel(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
