package com.example.iotmonitoring.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.iotmonitoring.model.NodeSensorModel;
import com.example.iotmonitoring.model.NodeTimestampModel;
import com.example.iotmonitoring.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class NodeTimestampAdapter extends
        RecyclerView.Adapter<NodeTimestampAdapter.NodeTimestampViewHolder> {
    private ArrayList<NodeTimestampModel> timestampList;
    private String nodeKey;
    private Context context;

    public NodeTimestampAdapter(ArrayList<NodeTimestampModel> timestampList,
                                String nodeKey, Context context) {
        this.timestampList = timestampList;
        this.nodeKey = nodeKey;
        this.context = context;
    }

    @NonNull
    @Override
    public NodeTimestampViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_timestamp,
                parent, false);
        return new NodeTimestampAdapter.NodeTimestampViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NodeTimestampViewHolder holder, int position) {
        final String Timestamp = timestampList.get(position).getTimestamp();
        holder.tvTimestamp.setText(Timestamp);

        holder.reference.child("users").child(holder.mAuth.getUid()).child("nodes").child(nodeKey)
                .child("data").child(Timestamp)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        holder.dataSensors = new ArrayList<>();
                        for (DataSnapshot sensorSnapshot : dataSnapshot.getChildren()) {
                            NodeSensorModel sensors = new NodeSensorModel(
                                    sensorSnapshot.getKey(), sensorSnapshot.getValue().toString()
                            );
                            holder.dataSensors.add(sensors);
                        }
                        holder.adapter = new NodeSensorAdapter(holder.dataSensors, context);
                        holder.rvSensor.setAdapter(holder.adapter);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Toast.makeText(holder.itemView.getContext(),
                                "Fail to load data", Toast.LENGTH_LONG).show();
                        Log.e("NodesActivity", databaseError.getDetails()
                                + " " + databaseError.getMessage());
                    }
                });
    }


    @Override
    public int getItemCount() {
        return timestampList.size();
    }

    public class NodeTimestampViewHolder extends RecyclerView.ViewHolder {
        private TextView tvTimestamp;
        private RecyclerView rvSensor;

        private FirebaseAuth mAuth;
        private FirebaseDatabase firebaseDatabase;
        private DatabaseReference reference;
        private ArrayList<NodeSensorModel> dataSensors;
        private NodeSensorAdapter adapter;
        private RecyclerView.LayoutManager layoutManager;

        public NodeTimestampViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTimestamp = itemView.findViewById(R.id.tv_item_timestamp);
            rvSensor = itemView.findViewById(R.id.rv_sensors);

            mAuth = FirebaseAuth.getInstance();
            firebaseDatabase = FirebaseDatabase.getInstance();
            reference = firebaseDatabase.getReference();
            myRecyclerView();
        }

        private void myRecyclerView() {
            layoutManager = new LinearLayoutManager(context,
                    LinearLayoutManager.HORIZONTAL, false);
            rvSensor.setLayoutManager(layoutManager);
            rvSensor.setHasFixedSize(true);
        }
    }
}
