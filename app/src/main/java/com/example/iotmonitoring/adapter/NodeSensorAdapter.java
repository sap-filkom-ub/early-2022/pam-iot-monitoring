package com.example.iotmonitoring.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.iotmonitoring.model.NodeSensorModel;
import com.example.iotmonitoring.R;

import java.util.ArrayList;

public class NodeSensorAdapter extends
        RecyclerView.Adapter<NodeSensorAdapter.NodeSensorViewHolder> {
    private ArrayList<NodeSensorModel> sensorList;
    private Context context;

    public NodeSensorAdapter(ArrayList<NodeSensorModel> sensorList, Context context) {
        this.sensorList = sensorList;
        this.context = context;
    }

    @NonNull
    @Override
    public NodeSensorViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sensor,
                parent, false);
        return new NodeSensorAdapter.NodeSensorViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NodeSensorViewHolder holder, int position) {
        final String Key = sensorList.get(position).getKey();
        final String Value = sensorList.get(position).getValue();

        holder.tvKey.setText(Key);
        holder.tvValue.setText(Value);
    }

    @Override
    public int getItemCount() {
        return sensorList.size();
    }

    public class NodeSensorViewHolder extends RecyclerView.ViewHolder {
        private TextView tvKey, tvValue;

        public NodeSensorViewHolder(@NonNull View itemView) {
            super(itemView);
            tvKey = itemView.findViewById(R.id.tv_item_sensor_key);
            tvValue = itemView.findViewById(R.id.tv_item_sensor_value);
        }
    }
}
