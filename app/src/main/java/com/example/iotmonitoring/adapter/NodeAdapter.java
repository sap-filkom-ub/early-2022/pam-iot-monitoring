package com.example.iotmonitoring.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;


import com.example.iotmonitoring.NodeDetailActivity;
import com.example.iotmonitoring.model.NodeModel;
import com.example.iotmonitoring.R;

import java.util.ArrayList;

public class NodeAdapter extends
        RecyclerView.Adapter<NodeAdapter.NodeViewHolder> {
    private ArrayList<NodeModel> nodeList;
    private Context context;

    public NodeAdapter(ArrayList<NodeModel> nodeList, Context context) {
        this.nodeList = nodeList;
        this.context = context;
    }

    @NonNull
    @Override
    public NodeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_node,
                parent, false);
        return new NodeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NodeViewHolder holder,
                                 @SuppressLint("RecyclerView") int position) {
        final String Key = nodeList.get(position).getKey();
        final String Name = nodeList.get(position).getName();
        final String Description = nodeList.get(position).getDescription();
        holder.tvKey.setText(Key);
        holder.tvName.setText(Name);
        holder.tvDescription.setText(Description);

        holder.cvItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), NodeDetailActivity.class);
                intent.putExtra(NodeDetailActivity.EXTRA_NODE, nodeList.get(position));
                view.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return nodeList.size();
    }

    public class NodeViewHolder extends RecyclerView.ViewHolder {
        private TextView tvKey, tvName, tvDescription;
        private CardView cvItem;

        public NodeViewHolder(@NonNull View itemView) {
            super(itemView);
            tvKey = itemView.findViewById(R.id.tv_item_key);
            tvName = itemView.findViewById(R.id.tv_item_name);
            tvDescription = itemView.findViewById(R.id.tv_item_description);
            cvItem = itemView.findViewById(R.id.cv_item_node);
        }
    }
}
